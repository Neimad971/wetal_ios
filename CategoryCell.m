//
//  CategoriesCell.m
//  Wetal
//
//  Created by Damien TALBOT on 26/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import "CategoryCell.h"

@implementation CategoryCell

- (void)awakeFromNib
{
    // Initialization code
    NSLog(@"CategoriesCell -> awakeFromNib");
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)configureCellForCategory:(WTCategory *)category
{
    self.categoryNameLabel.text = category.name;
    [self.categoryImageView setImage:category.image];
    [self.followersImageView setImage:[UIImage imageNamed:@"img-follower-grey"]];
    self.followersLabel.text = @"5,9K";
}


- (IBAction)followOrUnfollow:(UIButton *)sender
{
    NSLog(@"followOrUnfollow");
    
    /*
    UIImageView *imageview = [sender imageView];
    
    if ([imageview image] fi) 
    {
        <#statements#>
    }
    */
}

@end
