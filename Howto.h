//
//  Howto.h
//  Wetal
//
//  Created by Damien TALBOT on 14/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Howto : NSObject

@property (nonatomic,copy) NSNumber *howtoId;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *insight;
@property (nonatomic,copy) NSString *creator;
@property (nonatomic,copy) NSString *createdAt;
@property (nonatomic,strong) NSNumber *heartsCounter;
@property (nonatomic,strong) NSArray *products;
@property (nonatomic,strong) NSNumber *steps;


@end
