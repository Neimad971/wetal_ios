//
//  Howto.m
//  Wetal
//
//  Created by Damien TALBOT on 14/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import "Howto.h"

@implementation Howto


- (id)init
{
    if((self = [super init]))
    {
        self.howtoId = [[NSNumber alloc]initWithInt:0];
        self.title = @"No label";
        self.insight = @"No description";
        self.title = @"No author";
        self.createdAt = @"No date";
        self.heartsCounter = [[NSNumber alloc]initWithInt:0];
        self.products = [[NSArray alloc]init];
        self.steps = [[NSNumber alloc]initWithInt:0];
    }
    
    return  self;
}

@end
