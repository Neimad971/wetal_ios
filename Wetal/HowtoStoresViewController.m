//
//  HowtoStoresViewController.m
//  Wetal
//
//  Created by Damien TALBOT on 15/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import "HowtoStoresViewController.h"
#import "StoreCell.h"

@interface HowtoStoresViewController ()

@end


static NSString *CellIdentifier = @"StoreCell";


@implementation HowtoStoresViewController
{
    NSArray *_stores;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = 80;
    
    UINib *cellNib = [UINib nibWithNibName:CellIdentifier bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:CellIdentifier];
    
    _stores = @[@"MAC Studio Fix Fluid Foundation",
                @"Real techniques Triangle Foundation Brush",
                @"MakeUp ForEver Aqua Brow - Ash"
    ];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_stores count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"in cellForRowAtIndexPath");
    
    StoreCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSString *store = _stores[indexPath.row];
    [cell configureCellForStore:store andHowto:self.howto];
    
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
