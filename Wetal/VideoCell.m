//
//  VideoCell.m
//  
//
//  Created by Damien TALBOT on 08/10/2015.
//
//

#import "VideoCell.h"


@implementation VideoCell


- (void)awakeFromNib
{
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)configureCellForVideo:(NSString *)video andHowto:(Howto *)howto
{
    [self setPictureForVideo:video andHowto:howto];
}


- (void)setPictureForVideo:(NSString *)video andHowto:(Howto *)howto;
{
    int howtoId = [howto.howtoId intValue];
    
    if (howtoId == 1 || howtoId == 2 || howtoId == 3)
    {
        //for makeup
        
        if ([video isEqualToString:@"Video 1"])
        {
            [self.videoImageView setImage:[UIImage imageNamed:@"video1"]];
        }
        else if ([video isEqualToString:@"Video 2"])
        {
            [self.videoImageView setImage:[UIImage imageNamed:@"video 2"]];
        }
        else
        {
            [self.videoImageView setImage:[UIImage imageNamed:@"video3"]];
        }
    }
    else if (howtoId == 4 || howtoId == 5 || howtoId == 6)
    {
        //for cooking
        
        if ([video isEqualToString:@"Video 1"])
        {
            [self.videoImageView setImage:[UIImage imageNamed:@"vid1"]];
        }
        else if ([video isEqualToString:@"Video 2"])
        {
            [self.videoImageView setImage:[UIImage imageNamed:@"vid2"]];
        }
        else
        {
            [self.videoImageView setImage:[UIImage imageNamed:@"vid3"]];
        }
    }
    else
    {
        //for electronic
    }
}


@end