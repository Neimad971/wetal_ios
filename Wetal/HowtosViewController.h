//
//  HowtosViewController.h
//  Wetal
//
//  Created by Damien TALBOT on 27/08/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WTCategory.h"

@interface HowtosViewController : UITableViewController


@property (nonatomic,strong) WTCategory *category;

- (IBAction)addNewHowto;


@end
