//
//  HowtoVideosTableViewController.h
//  Wetal
//
//  Created by Damien TALBOT on 15/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Howto.h"

@interface HowtoVideosViewController : UITableViewController

@property (nonatomic,strong) Howto *howto;

@end
