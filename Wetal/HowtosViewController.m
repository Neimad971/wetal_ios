//
//  HowtosViewController.m
//  Wetal
//
//  Created by Damien TALBOT on 27/08/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import "HowtosViewController.h"
#import "Howto.h"
#import "HowtoDescriptionViewController.h"
#import "HowtoCell.h"
#import "HowtoVideosViewController.h"

@interface HowtosViewController ()



@end


static NSString *HowtoCellIdentifier = @"HowtoCell";


static NSString *LoremIpsum = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.";


@implementation HowtosViewController
{
    NSArray *_howtos;
    Howto *_currentlySelectedHowto;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.category.name;
    
    self.tableView.rowHeight = 80;
    
    UINib *cellNib = [UINib nibWithNibName:HowtoCellIdentifier bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:HowtoCellIdentifier];
    
    [self setUpFakeData];
    
    }


- (void)setUpFakeData
{
    Howto *howto1 = [[Howto alloc]init];
    howto1.howtoId = [[NSNumber alloc]initWithInt:1];
    howto1.title = @"Day out make up";
    howto1.insight = LoremIpsum;
    howto1.creator = @"Trisha";
    howto1.createdAt = @"2 days ago";
    howto1.heartsCounter = [[NSNumber alloc]initWithInt:34];
    howto1.products = @[@"mac soft",@"Too face neutral",@"Lena lash in Iman"];
    howto1.steps = [[NSNumber alloc]initWithInt:6];
    
    
    
    Howto *howto2 = [[Howto alloc]init];
    howto2.howtoId = [[NSNumber alloc]initWithInt:2];
    howto2.title = @"French manicure EASY, CHEAP and FAST";
    howto2.insight = LoremIpsum;
    howto2.creator = @"Anna";
    howto2.createdAt = @"2 days ago";
    howto2.heartsCounter = [[NSNumber alloc]initWithInt:434];
    howto2.products = @[@"mascara",@"eye liner",@"brush"];
    howto2.steps = [[NSNumber alloc]initWithInt:3];
    
    
    Howto *howto3 = [[Howto alloc]init];
    howto3.howtoId = [[NSNumber alloc]initWithInt:3];
    howto3.title = @"Bright skin for white women";
    howto3.insight = LoremIpsum;
    howto3.creator = @"Lisa";
    howto3.createdAt = @"2 days ago";
    howto3.heartsCounter = [[NSNumber alloc]initWithInt:987];
    howto3.products = @[@"Lena lash in Iman",@"eye liner",@"Too face neutral"];
    howto3.steps = [[NSNumber alloc]initWithInt:4];
    
    
    Howto *howto4 = [[Howto alloc]init];
    howto4.howtoId = [[NSNumber alloc]initWithInt:4];
    howto4.title = @"Carbonara pasta";
    howto4.insight = LoremIpsum;
    howto4.creator = @"Gaia";
    howto4.createdAt = @"2 days ago";
    howto4.heartsCounter = [[NSNumber alloc]initWithInt:3242];
    howto4.products = @[@"Pasta",@"eggs",@"ham"];
    howto4.steps = [[NSNumber alloc]initWithInt:2];
    
    
    Howto *howto5 = [[Howto alloc]init];
    howto5.howtoId = [[NSNumber alloc]initWithInt:5];
    howto5.title = @"Fish and chips";
    howto5.insight = LoremIpsum;
    howto5.creator = @"John";
    howto5.createdAt = @"2 days ago";
    howto5.heartsCounter = [[NSNumber alloc]initWithInt:98];
    howto5.products = @[@"Fish",@"Chips"];
    howto5.steps = [[NSNumber alloc]initWithInt:4];
    
    
    Howto *howto6 = [[Howto alloc]init];
    howto6.howtoId = [[NSNumber alloc]initWithInt:6];
    howto6.title = @"Cake";
    howto6.insight = LoremIpsum;
    howto6.creator = @"Damien";
    howto6.createdAt = @"2 days ago";
    howto6.heartsCounter = [[NSNumber alloc]initWithInt:323];
    howto6.products = @[@"Rice",@"colombo powder",@"chicken"];
    howto6.steps = [[NSNumber alloc]initWithInt:5];
    
    
    Howto *howto7 = [[Howto alloc]init];
    howto7.howtoId = [[NSNumber alloc]initWithInt:7];
    howto7.title = @"Basic Electronic Components and their Symbols";
    howto7.insight = LoremIpsum;
    howto7.creator = @"Dave";
    howto7.createdAt = @"2 days ago";
    howto7.heartsCounter = [[NSNumber alloc]initWithInt:8253];
    howto7.products = @[@"piece of paper",@"pen"];
    howto7.steps = [[NSNumber alloc]initWithInt:8];
    
    
    Howto *howto8 = [[Howto alloc]init];
    howto8.howtoId = [[NSNumber alloc]initWithInt:8];
    howto8.title = @"Digital Electronics: Logic Gates - Part 1";
    howto8.insight = LoremIpsum;
    howto8.creator = @"Derek";
    howto8.createdAt = @"2 days ago";
    howto8.heartsCounter = [[NSNumber alloc]initWithInt:4];
    howto8.products = @[@"pic controlle",@"motor step by step",@"batery"];
    howto8.steps = [[NSNumber alloc]initWithInt:10];
    
    
    Howto *howto9 = [[Howto alloc]init];
    howto9.howtoId = [[NSNumber alloc]initWithInt:9];
    howto9.title = @"How a Light Emitting Diode Works";
    howto9.insight = LoremIpsum;
    howto9.creator = @"Mike";
    howto9.createdAt = @"2 days ago";
    howto9.heartsCounter = [[NSNumber alloc]initWithInt:83];
    howto9.products = @[@"diode",@"batery 12V"];
    howto9.steps = [[NSNumber alloc]initWithInt:9];
    
    
    if ([self.category.name isEqualToString:@"Make up"])
    {
        _howtos = @[howto1,howto2,howto3];
    }
    else if ([self.category.name isEqualToString:@"Cooking"])
    {
        _howtos = @[howto4,howto5,howto6];
    }
    else
    {
        _howtos = @[howto7,howto8,howto9];
    }
}


- (IBAction)addNewHowto
{
    NSLog(@"addNewHowto");
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_howtos count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"in cellForRowAtIndexPath");
    
    HowtoCell *cell = [tableView dequeueReusableCellWithIdentifier:HowtoCellIdentifier forIndexPath:indexPath];
    
    Howto *howto = _howtos[indexPath.row];
    [cell configureCellForHowto:howto];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"HowtoDetail" sender:nil];
}


- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    Howto *howto = _howtos[indexPath.row];
    _currentlySelectedHowto = howto;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"HowtoDetail"])
    {
        
        UITabBarController *tabBarController = (UITabBarController *)segue.destinationViewController;
        
        HowtoDescriptionViewController *howtoDescriptionViewController = (HowtoDescriptionViewController *)tabBarController.viewControllers[0];
        
        howtoDescriptionViewController.howto = _currentlySelectedHowto;
        
        //I'm not sure about this line
        tabBarController.selectedViewController = howtoDescriptionViewController;
    
    }
}


@end
