//
//  VideoCell.h
//  
//
//  Created by Damien TALBOT on 08/10/2015.
//
//

#import <UIKit/UIKit.h>
#import "Howto.h"


@interface VideoCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;

- (void)configureCellForVideo:(NSString *)video andHowto:(Howto *)howto;


@end
