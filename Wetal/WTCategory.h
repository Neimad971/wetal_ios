//
//  Category.h
//  Wetal
//
//  Created by Damien TALBOT on 13/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WTCategory : NSObject


@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) UIImage *image;


@end
