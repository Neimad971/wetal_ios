//
//  AppDelegate.h
//  Wetal
//
//  Created by Damien TALBOT on 27/08/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CategoriesIntViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;


@end

