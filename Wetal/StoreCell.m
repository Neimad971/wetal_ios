//
//  StoreCell.m
//  Wetal
//
//  Created by Damien TALBOT on 30/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import "StoreCell.h"

@implementation StoreCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)configureCellForStore:(NSString *)store andHowto:(Howto *)howto
{
    self.storeNameLabel.text = store;
    self.webSiteUrlLabel.text = @"www.boots.com";
    self.addressLabel.text = @"282 Baker St, London W1U, United Kingdom";
    [self setPictureForStore:store andHowto:howto];
    [self.webSiteImageView setImage:[UIImage imageNamed:@"globe"]];
    [self.addressImageView setImage:[UIImage imageNamed:@"location"]];
}


- (void)setPictureForStore:(NSString *)store andHowto:(Howto *)howto
{
    int howtoId = [howto.howtoId intValue];
    
    if (howtoId == 1 || howtoId == 2 || howtoId == 3)
    {
        if ([store isEqualToString:@"MAC Studio Fix Fluid Foundation"])
        {
            [self.storeImageView setImage:[UIImage imageNamed:@"product2-diy"]];
        }
        else if ([store isEqualToString:@"Real techniques Triangle Foundation Brush"])
        {
            [self.storeImageView setImage:[UIImage imageNamed:@"product-makeup"]];
        }
        else
        {
            [self.storeImageView setImage:[UIImage imageNamed:@"img-product"]];
        }
    }
    else if (howtoId == 4 || howtoId == 5 || howtoId == 6)
    {
        if ([store isEqualToString:@"MAC Studio Fix Fluid Foundation"])
        {
            [self.storeImageView setImage:[UIImage imageNamed:@"product1"]];
        }
        else if ([store isEqualToString:@"Real techniques Triangle Foundation Brush"])
        {
            [self.storeImageView setImage:[UIImage imageNamed:@"product2"]];
        }
        else
        {
            [self.storeImageView setImage:[UIImage imageNamed:@"product3"]];
        }
    }
}

@end
