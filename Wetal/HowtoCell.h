//
//  HowtoCell.h
//  Wetal
//
//  Created by Damien TALBOT on 29/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Howto.h"

@interface HowtoCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UILabel *howtoTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *howtoDateLabel;
@property (nonatomic, weak) IBOutlet UILabel *howtoCreatorLabel;
@property (nonatomic, weak) IBOutlet UILabel *howtoNbLikeLabel;
@property (nonatomic, weak) IBOutlet UIImageView *howtoPictureImageView;
@property (nonatomic, weak) IBOutlet UIImageView *howtoDateImageView;
@property (nonatomic, weak) IBOutlet UIImageView *howtoCreatorImageView;
@property (nonatomic, weak) IBOutlet UIImageView *howtoNbLikeImageView;

- (void)configureCellForHowto:(Howto *)howto;

@end
