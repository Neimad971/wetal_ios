//
//  StoreCell.h
//  Wetal
//
//  Created by Damien TALBOT on 30/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Howto.h"

@interface StoreCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *storeNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *webSiteUrlLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UIImageView *storeImageView;
@property (nonatomic, weak) IBOutlet UIImageView *webSiteImageView;
@property (nonatomic, weak) IBOutlet UIImageView *addressImageView;


// the may change according to be a proper Store object...let's see
- (void)configureCellForStore:(NSString *)store andHowto:(Howto *)howto;

@end
