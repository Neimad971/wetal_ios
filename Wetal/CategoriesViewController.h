//
//  AllCategoriesViewController.h
//  Wetal
//
//  Created by Damien TALBOT on 27/08/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesViewController : UITableViewController

- (IBAction)addNewCategory;

@end
