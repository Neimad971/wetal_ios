//
//  AllCategoriesViewController.m
//  Wetal
//
//  Created by Damien TALBOT on 27/08/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import "CategoriesViewController.h"
#import "HowtosViewController.h"
#import "WTCategory.h"
#import "CategoryCell.h"

@interface CategoriesViewController ()

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@end


static NSString *CategoryCellIdentifier = @"CategoryCell";


@implementation CategoriesViewController
{
    NSArray *_categories;
    WTCategory *_currentlySelectedCategory;
}


- (void)viewDidLoad
{
    NSLog(@"in viewDidLoad");
    
    [super viewDidLoad];

    
    self.tableView.rowHeight = 115;
    
    UINib *cellNib = [UINib nibWithNibName:CategoryCellIdentifier bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:CategoryCellIdentifier];
    
    WTCategory *category1 = [[WTCategory alloc]init];
    category1.name = @"Cooking";
    category1.image = [UIImage imageNamed:@"food"];
    
    WTCategory *category2 = [[WTCategory alloc]init];
    category2.name = @"Electronic";
    category2.image = [UIImage imageNamed:@"electronics"];
    
    WTCategory *category3 = [[WTCategory alloc]init];
    category3.name = @"Make up";
    category3.image = [UIImage imageNamed:@"makeup"];
    
    
    _categories = @[category1,category2,category3];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"in numberOfRowsInSection");
    
    return [_categories count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"in cellForRowAtIndexPath");
    CategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:CategoryCellIdentifier
                                                            forIndexPath:indexPath];
    
    WTCategory *category = _categories[indexPath.row];
    [cell configureCellForCategory:category];
    
    return cell;
}


- (IBAction)addNewCategory
{
    NSLog(@"addNewCategory");
}


#pragma mark - UITableViewDelegate

//triggered first
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"in didHighlightRowAtIndexPath");
    WTCategory *category = _categories[indexPath.row];
    _currentlySelectedCategory = category;
}


//triggered second
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"showHowtoList" sender:self];
}


//triggered third
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showHowtoList"])
    {
        HowtosViewController *controller = segue.destinationViewController;
        controller.category = _currentlySelectedCategory;
    }
}



#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self performSearch];
}


- (void)performSearch
{
    if ([self.searchBar.text length] > 0)
    {
        [self.searchBar resignFirstResponder];
        
        // ...
    }
}


@end