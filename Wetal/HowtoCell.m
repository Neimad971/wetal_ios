//
//  HowtoCell.m
//  Wetal
//
//  Created by Damien TALBOT on 29/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import "HowtoCell.h"

@implementation HowtoCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)configureCellForHowto:(Howto *)howto
{
    self.howtoTitleLabel.text = howto.title;
    self.howtoDateLabel.text = howto.createdAt;
    self.howtoCreatorLabel.text = howto.creator;
    self.howtoNbLikeLabel.text = [NSString stringWithFormat:@"%d", [howto.heartsCounter intValue]];
    [self setPictureForHowto:howto];
    [self.howtoDateImageView setImage:[UIImage imageNamed:@"sablier"]];
    [self.howtoCreatorImageView setImage:[UIImage imageNamed:@"user"]];
    [self.howtoNbLikeImageView setImage:[UIImage imageNamed:@"heart"]];
}


- (void)setPictureForHowto:(Howto *)howto
{
    switch ([howto.howtoId intValue])
    {
        case 1:
            [self.howtoPictureImageView setImage:[UIImage imageNamed:@"diy-makeup3"]];
            break;
            
            
        case 2:
            [self.howtoPictureImageView setImage:[UIImage imageNamed:@"makeup-diy"]];
            break;
            
            
        case 3:
            [self.howtoPictureImageView setImage:[UIImage imageNamed:@"diy-makeup2"]];
            break;
            
            
        case 4:
            [self.howtoPictureImageView setImage:[UIImage imageNamed:@"diy-food1"]];
            break;
            
            
        case 5:
            [self.howtoPictureImageView setImage:[UIImage imageNamed:@"diy-food2"]];
            break;
            
            
        case 6:
            [self.howtoPictureImageView setImage:[UIImage imageNamed:@"cake-howto"]];
            break;
            
            
        case 7:
            [self.howtoPictureImageView setImage:[UIImage imageNamed:@"elec-diy4"]];
            break;
            
            
        case 8:
            [self.howtoPictureImageView setImage:[UIImage imageNamed:@"diy-elec2"]];
            break;
            
            
        case 9:
            [self.howtoPictureImageView setImage:[UIImage imageNamed:@"diy-elec3"]];
            break;
            
            
        default:
            break;
    }
}

@end
