//
//  HowtoVideosTableViewController.m
//  Wetal
//
//  Created by Damien TALBOT on 15/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "HowtoVideosViewController.h"
#import "VideoCell.h"


@interface HowtoVideosViewController ()

@property (nonatomic, strong) NSArray *videos;

@end


static NSString *VideoCellIdentifier = @"VideoCell";


static NSString *YTLink = @"https://www.youtube.com/watch?v=aQYHxedVl6c";



@implementation HowtoVideosViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = 115;
    
    UINib *cellNib = [UINib nibWithNibName:VideoCellIdentifier bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:VideoCellIdentifier];
    
    _videos = @[@"Video 1", @"Video 2", @"Video 3"];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_videos count];
}



- (VideoCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"in cellForRowAtIndexPath");
    
    VideoCell *cell = (VideoCell *)[tableView dequeueReusableCellWithIdentifier:VideoCellIdentifier
                                                                   forIndexPath:indexPath];
    NSString *video = _videos[indexPath.row];
    [cell configureCellForVideo:video andHowto:self.howto];

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self playMovie];
    
    NSLog(@"in didHighlightRowAtIndexPath");
}


- (void)playMovie
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"30s" ofType:@"mp4"];
    
    if (path != nil)
    {
        NSURL *videoURL = [NSURL fileURLWithPath:path];
        AVPlayer *player = [AVPlayer playerWithURL:videoURL];
        AVPlayerViewController *playerViewController = [AVPlayerViewController new];
        playerViewController.player = player;
        [playerViewController.player play];//Used to Play On start
        [self presentViewController:playerViewController animated:YES completion:nil];
    }
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
