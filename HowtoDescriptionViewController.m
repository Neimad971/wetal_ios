//
//  HowtoDescriptionViewController.m
//  Wetal
//
//  Created by Damien TALBOT on 14/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import "HowtoDescriptionViewController.h"
#import "HowtoVideosViewController.h"
#import "HowtoStoresViewController.h"

@interface HowtoDescriptionViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *createdAtImageView;
@property (weak, nonatomic) IBOutlet UIImageView *creatorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *heartsAtImageView;
@property (weak, nonatomic) IBOutlet UILabel *createdAtLabel;
@property (weak, nonatomic) IBOutlet UILabel *creatorLabel;
@property (weak, nonatomic) IBOutlet UILabel *heartsLabel;

@end


static NSString *LoremIpsum = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.";



@implementation HowtoDescriptionViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.tabBarController.delegate = self;
    
    [self setUpView];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setUpView
{
    // title of the screen
    self.tabBarController.title = self.howto.title;
    
    // on the 2nd navigationbar
    [self.createdAtImageView setImage:[UIImage imageNamed:@"sablier"]];
    [self.creatorImageView setImage:[UIImage imageNamed:@"user"]];
    [self.heartsAtImageView setImage:[UIImage imageNamed:@"heart"]];
    self.createdAtLabel.text = self.howto.createdAt;
    self.creatorLabel.text = self.howto.creator;
    self.heartsLabel.text = [NSString stringWithFormat:@"%d", [self.howto.heartsCounter intValue]];
    
    [self setUpScrollView];
}

- (void)setUpScrollView
{
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    //scrollView.contentInset=UIEdgeInsetsMake(108.0,0.0,44.0,0.0);

    //scrollView.backgroundColor = [UIColor redColor];
    scrollView.scrollEnabled = YES;
    scrollView.showsVerticalScrollIndicator = YES;
    scrollView.contentSize = CGSizeMake(self.view.bounds.size.width , self.view.bounds.size.height * 4);
    [self.view addSubview:scrollView];
    
    float width = self.view.bounds.size.width;
    float height = 45;
    float xPos = 8;
    float yPos = 110;

    
    //insightTitle
    UILabel *insightTitle = [[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, width, height)];
    insightTitle.text = @"Presentation";
    [scrollView addSubview:insightTitle];
    
    
    //insightLabel
    UILabel *insightLabel = [[UILabel alloc]initWithFrame:
                             CGRectMake(xPos, yPos + height, width, height*3)];
    insightLabel.font = [UIFont systemFontOfSize:14];
    //insightLabel.textColor = [UIColor grayColor];
    insightLabel.numberOfLines = 10;
    insightLabel.lineBreakMode = NSLineBreakByWordWrapping;
    insightLabel.text = self.howto.insight;
    [scrollView addSubview:insightLabel];
    
    
    //productsTitle
    UILabel *productsTitle = [[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos + height*5, width, height)];
    productsTitle.text = @"Products";
    [scrollView addSubview:productsTitle];
    
    
    //productsLabel
    UILabel *productsLabel = [[UILabel alloc]initWithFrame:
                              CGRectMake(xPos, yPos + height*6, width, height)];
    productsLabel.font = [UIFont systemFontOfSize:14];
    
    productsLabel.text = self.howto.products[0];
    
    int i = 1;
    
    if (self.howto.products.count > 1)
    {
        while (i<self.howto.products.count)
        {
            productsLabel.text = [productsLabel.text stringByAppendingString:@", "];
            productsLabel.text = [productsLabel.text stringByAppendingString:self.howto.products[i]];
            i++;;
        }
    }

    [scrollView addSubview:productsLabel];
    
    
    //productsImageView
    UIImageView *productsImageView = [[UIImageView alloc]initWithFrame:CGRectMake(xPos, yPos + height*7, width, 246/*height*3*/)];
    
    int howtoId = [self.howto.howtoId intValue];
    
    if (howtoId == 1 || howtoId == 2 || howtoId == 3)
    {
        //productsImageView for makeup
        [productsImageView setImage:[UIImage imageNamed:@"products320x246"]];
    }
    else if(howtoId == 4 || howtoId == 5 || howtoId == 6)
    {
        //productsImageView for cooking
        [productsImageView setImage:[UIImage imageNamed:@"diy cake"]];
    }
    else
    {
        //productsImageView for electronic
        
    }
    
    [scrollView addSubview:productsImageView];
    
    
    
    //stepTitle & steplabel & stepImageView
    int stepTitleYPos = yPos + height*11;
    int stepLabelYPos = yPos + height*12;
    int stepImageViewYPos = yPos + height*13;
    
    for (int i=1; i<=[self.howto.steps intValue]; i++)
    {
        //stepTitle
        UILabel *stepTitle = [[UILabel alloc]initWithFrame:CGRectMake(xPos, stepTitleYPos, width, height)];
        stepTitle.text = [NSString stringWithFormat:@"Step %d/%d", i, [self.howto.steps intValue]];
        [scrollView addSubview:stepTitle];
        stepTitleYPos += 350;
        
        //steplabel
        UILabel *steplabel = [[UILabel alloc]initWithFrame:CGRectMake(xPos, stepLabelYPos, width, height)];
        steplabel.font = [UIFont systemFontOfSize:14];
        steplabel.numberOfLines = 10;
        steplabel.lineBreakMode = NSLineBreakByWordWrapping;
        steplabel.text = LoremIpsum;
        [scrollView addSubview:steplabel];
        stepLabelYPos += 350;
        
        
        UIImageView *stepImageView = [[UIImageView alloc]initWithFrame:CGRectMake(xPos, stepImageViewYPos, width, 246/*height*3*/)];
       
        if (howtoId == 1 || howtoId == 2 || howtoId == 3)
        {
            //stepImageView for electronic
            
            if(i == 1)
            {
                [stepImageView setImage:[UIImage imageNamed:@"img tuto2"]];
            }
            else if(i == 2)
            {
                [stepImageView setImage:[UIImage imageNamed:@"img tuto1"]];
            }
            else
            {
                [stepImageView setImage:[UIImage imageNamed:@"img tuto3"]];
            }
        }
        else if(howtoId == 4 || howtoId == 5 || howtoId == 6)
        {
            //stepImageView for cooking
            
            if(i == 1)
            {
                [stepImageView setImage:[UIImage imageNamed:@"desc-cake1"]];
            }
            else if(i == 2)
            {
                [stepImageView setImage:[UIImage imageNamed:@"desc-cake2"]];
            }
            else if(i == 3)
            {
                [stepImageView setImage:[UIImage imageNamed:@"desc-cake3"]];
            }
            else if(i == 4)
            {
                [stepImageView setImage:[UIImage imageNamed:@"desc-cake4"]];
            }
            else
            {
                [stepImageView setImage:[UIImage imageNamed:@"desc-cake5"]];
            }

        }
        else
        {
            //stepImageView for electronic
        }
        
        
        [scrollView addSubview:stepImageView];
        stepImageViewYPos += 350;
    }
    
    NSLog(@"setUpScrollView");
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    
    NSLog(@"self.howto --> %d", [self.howto.howtoId intValue]);
    NSLog(@"tabBarController --> %lu", (unsigned long)tabBarController.selectedIndex);
    

    
    UINavigationController *navController = [tabBarController selectedViewController];
    
    int selectedIndex = (int)tabBarController.selectedIndex;
    
    if (selectedIndex == 1)
    {
        HowtoVideosViewController *howtoVideosViewController = (HowtoVideosViewController *)navController.topViewController;
        howtoVideosViewController.howto = self.howto;
    }
    else if(selectedIndex == 2)
    {
        HowtoStoresViewController *howtoStoresViewController = (HowtoStoresViewController *)navController.topViewController;
        howtoStoresViewController.howto = self.howto;
    }
    

    
;
    
    
    
}



#pragma mark - Navigation

/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
