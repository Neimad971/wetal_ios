//
//  CategoriesCell.h
//  Wetal
//
//  Created by Damien TALBOT on 26/09/2015.
//  Copyright (c) 2015 SITALIX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WTCategory.h"

@interface CategoryCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *categoryNameLabel;
@property (nonatomic, weak) IBOutlet UIImageView *categoryImageView;
@property (nonatomic, weak) IBOutlet UIImageView *followersImageView;
@property (nonatomic, weak) IBOutlet UILabel *followersLabel;
@property (nonatomic, weak) IBOutlet UIButton *followButton;

- (void)configureCellForCategory:(WTCategory *)category;

@end
